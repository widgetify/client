# **Widgetify**
` `

### Widgetify is React web application. The web app contains all kinds of widgets that the user can choose to "install" to display on that webpage. Ideally it can serve as a homepage for the user.


The idea is that a user wouldn't need to hop around the web opening new tabs every time but to have a centralized website where most services the user needs can be found.


## **Microservice Architecture**
` `

Because of the app is composed of different widgets it made sense to use a microservice architecture to be able to mantain each sub-app (or widget) individually.

From a software architecture perspective the widgets can be divided in three categories, namely:



**1. Microservices**


**2. Cloud functions**


**3. Browser extensions**




We used docker and kubernetes in GCP to deploy and maintain the microservice architecture.


## **1. Microservices**
` `


**Frontend Microservice:**

[This repository](https://bitbucket.org/widgetify/client)

**Authentication:**

[https://bitbucket.org/widgetify/server/src/master/](https://bitbucket.org/widgetify/server/src/master/)

**What-to-do Widget:**

[https://bitbucket.org/widgetify/whattodo/src/master/](https://bitbucket.org/widgetify/whattodo/src/master/)

**Timezone Time Widget:**

[https://bitbucket.org/widgetify/timezone/src/master/](https://bitbucket.org/widgetify/timezone/src/master/)


## ** 2. Google Cloud Functions **
` `

**Nature Images Widget:**

[https://bitbucket.org/widgetify/widget-nature-images/src/master/](https://bitbucket.org/widgetify/widget-nature-images/src/master/)

**Golang Calculator:**

[https://bitbucket.org/widgetify/widget-golang-calculator/src/master/](https://bitbucket.org/widgetify/widget-golang-calculator/src/master/)


## ** 3. Browser Extensions **
` `

**Chrome TopSites:**

[https://bitbucket.org/widgetify/chrome-extension-topsites/src/master/](https://bitbucket.org/widgetify/chrome-extension-topsites/src/master/)


### `Techstack`

- React
- GraphQL
- node/express
- TailwindCSS
- Docker
- Google Cloud Platform
- Bitbucket Pipelines

### `How to Run Locally`

To run this part of the application (frontend), clone the repo, then `$ cd client` and use the command `$ yarn start`. 

To run the full architecture locally the other microservices (**Authentication, What-to-do Widget and Timezone Time Widget**) should also be cloned.

Aditionally, a .env file should be created inside the root folder of this repository with the pertinent variables.


### `What Alejandro Worked On`

- Node.js authentication
- Node.js password reset mechanism
- Email authentication for signup
- Frontend (signup/login, general setup, widgets, design implementation)
- GCP kubernetes deployments (authentication, nature images widget)
- Chrome Extensions (chrome top sites widget)
- GCP Cloud Functions (calculator, nature images widget)