import React, {useState} from 'react';
import axios from 'axios';
import { Redirect } from 'react-router'
import {Link} from 'react-router-dom'
import InputField from './InputField'
import Button from './Button'
import SubtleMessge from './SubtleMessge'
import AlertBox from './AlertBox'
import { createBrowserHistory } from "history"

const history = createBrowserHistory();

const NewPasswordForm = (props) => {
  const token = props.match.params.token 
  const id = props.match.params.id 

  const [form, setValues] = useState({
    password: "",
    id: id,
    token: token
    // id: "",
    // token: ""
  });
  
  const [errors, setErrors] = useState(null);

  const [redirect, setRedirect] = useState(false);

  const updateField = (e) => setValues({
    ...form,
    [e.target.name]: e.target.value
  })

  const submitHandler = e => {
    e.preventDefault();
    axios.post('http://' + process.env.REACT_APP_NODE_AUTH_URL + '/api/user/newpassword', form)
      .then(response => {
        if (response.data.error){
          console.log('response.data.error:', response.data.error)
          // setErrors(response.data.error)
        }
        else{
          console.log(response.data)
          localStorage.setItem('usertoken', response.data)
          setRedirect(true)
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  if (redirect === true) {
    return <Redirect to = '/'/>
  }

  return(
  <div className="flex">
    <div className="w-2/6"></div>
    <div className="my-24 w-2/6">
      <h1>Enter new password</h1>
      <Link className = "signup-link" to = '/signup'><SubtleMessge>Don't need a new password? Click here</SubtleMessge></Link>
      <form onSubmit={submitHandler}>
          
        {/* <InputField label='token' name='token' type="hidden" value={token}></InputField>
        <InputField label='id' name='id' type="hidden" value={id}></InputField> */}
        <InputField label='Password' name='password' placeholder='new password' type="password" value={form.password} onChange={updateField}></InputField>

        <Button type='submit'>Login</Button>
      </form>
    </div>
    <div className="w-2/6">
      {(errors)?
        <AlertBox>
            <ul>
            {(errors && errors.email)?<p color = "pink">{errors.email}</p>: null}
            {(errors && errors.password)?<p color = "pink">{errors.password}</p>: null}
            </ul>
        </AlertBox>
      : null}

    </div>
  </div>
  )
}

export default NewPasswordForm





        