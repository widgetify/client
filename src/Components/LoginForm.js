import React, {useState} from 'react';
import axios from 'axios';
import { Redirect } from 'react-router'
import {Link} from 'react-router-dom'
import InputField from './InputField'
import Button from './Button'
import SubtleMessge from './SubtleMessge'
import AlertBox from './AlertBox'
import { createBrowserHistory } from "history"

const history = createBrowserHistory();

const LoginForm = () => {

  const [form, setValues] = useState({
    email: '',
    password: '',
  });
  const [errors, setErrors] = useState(null);

  const updateField = (e) => setValues({
    ...form,
    [e.target.name]: e.target.value
  })

  const submitHandler = e => {
    e.preventDefault();
    axios.post('http://' + process.env.REACT_APP_NODE_AUTH_URL + '/api/user/login', form)
      .then(response => {
        console.log(response.data)
        if (response.data.error){
          setErrors(response.data.error)
        }
        else{
          console.log(response.data)
          localStorage.setItem('usertoken', response.data)
          return history.goBack();
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

    
    if (localStorage.usertoken){
      return <Redirect to = '/' />
    }

  return(
  <div className="flex">
    <div className="w-2/6"></div>
    <div className="my-24 w-2/6">
      <h1>Log In</h1>
      <Link className = "signup-link" to = '/signup'><SubtleMessge>Haven't joined yet? Click here to Sign up</SubtleMessge></Link>
      <form onSubmit={submitHandler}>
        <InputField label='Email' name='email' placeholder='email' type="email" value={form.email} onChange={updateField}></InputField>
          
        <InputField label='Password' name='password' placeholder='Password' type="password" value={form.password} onChange={updateField}></InputField>

        <Button type='submit'>Login</Button>
      </form>
    </div>
    <div className="w-2/6">
      {(errors)?
        <AlertBox>
            <ul>
            {(errors && errors.email)?<p color = "pink">{errors.email}</p>: null}
            {(errors && errors.password)?<p color = "pink">{errors.password}</p>: null}
            </ul>
        </AlertBox>
      : null}

    </div>
  </div>
  )
}

export default LoginForm