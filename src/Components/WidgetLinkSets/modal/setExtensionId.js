import React, { useState } from 'react';
import { Modal as AntModal, Form, Input, Button, Select } from 'antd';

export default ({toggleSetChromeExtensionIdModal, visible}) => {

    const [form] = Form.useForm()

    const [formData, setFormData] = useState({chromeextensionid: ''})

    const saveChromeExtensionId = (id) => {
        localStorage.setItem('widget-linkset-chromeId', id)
        toggleSetChromeExtensionIdModal()
    }

    const handleFormValueChange = (changedValues, allValues) => {
        setFormData( prevState => ({
            ...prevState,
            ...changedValues
        }))
    }

    return <AntModal
            visible={visible}
            title="Set Chrome Extension Id"
            onCancel={() => { toggleSetChromeExtensionIdModal() }}
            footer={[
                <Button key="back" onClick={toggleSetChromeExtensionIdModal}>
                Cancel
                </Button>,
                <Button key="submit" onClick={() => { saveChromeExtensionId(formData.chromeextensionid) } } type="primary">
                Save
                </Button>,
            ]}
        >
            <Form 
                form={form}
                initialValues={formData}
                onValuesChange={(changedValues, allValues) => { handleFormValueChange(changedValues, allValues) }}
                name="control-hooks">
                <Form.Item name="chromeextensionid" label="Chrome Extension Id" rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
            </Form>
        </AntModal>
}