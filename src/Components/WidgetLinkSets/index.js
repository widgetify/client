import React, {Component, useState, useEffect} from 'react';
import { MuuriComponent, useRefresh } from "muuri-react";
import { faExternalLinkAlt, faEdit, faTrash, faWindowClose, faPlusSquare, faSave } from "@fortawesome/free-solid-svg-icons";
import { faChrome } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Modal from 'react-modal';
import '../../styles/modal.scss'
import './styles/linkset.scss'
import { openTabs } from './utils'
import Window from './modules/Window'
import './modules/LinkItem'
import SetExtensionId from './modal/setExtensionId'

const WidgetLinkSets = () => {

    let [modalIsOpen, setModalIsOpen]                       = useState(false);
    let [currentLinkset, setCurrentLinkset]                 = useState(0);
    let [currentTrigger, setTrigger]                        = useState(0);
    let [chromeExtensionIdModal, setChromeExtensionIdModal] = useState(false);
    let [editLinksetName, setEditLinksetName]               = useState(false);
    let [linkSets, setLinksets]                             = useState( [
        {
            name: "First Linkset",
            windows: [
                {
                    name: 'first',
                    links: [
                        "https://google.com"
                    ]
                }
            ]
        }
    ]);

    useRefresh([currentTrigger]);

    useEffect(() => {
        Modal.setAppElement('body');
        loadLinksets()
    }, [])

    let newName = ""

    const toggleSetChromeExtensionIdModal = () => {
        setChromeExtensionIdModal(!chromeExtensionIdModal)
    }

    const boardOptions = {
        //layoutDuration: 400,
        dragEnabled: true,
        dragFixed: true,
        dragStartPredicate: {
            handle: ".window-header"
        },
        dragRelease: {
            duration: 400,
            easing: "ease-out"
        },
        layout: {
            horizontal: true,
        },
        dragPlaceholder: {
            enabled: true,
            createElement: function(item) {
              // The element will have the Css class ".muuri-item-placeholder".
              return item.getElement().cloneNode(true);
            }
          },
        containerClass: "flex"
    };

    const loadLinksets = () => {

        const linksets = JSON.parse(localStorage.getItem('linksets'))
        console.log(linksets)
        if (localStorage.getItem('linksets')) setLinksets(linksets);
    }

    const updateLinkset = (linksets) => {
        localStorage.setItem('linksets', JSON.stringify(linksets));
    }

    const openModal = (currentIndex) => {
        // set current linkset and open modal
        setModalIsOpen(true)
        setCurrentLinkset(currentIndex)
    }

    const afterOpenModal = () => {
        // references are now sync'd and can be accessed.
        // subtitle.style.color = 'bg-color';
    }

    const closeModal = () => {
        setModalIsOpen(false)
    }


    const addWindow = () => {
        const newLinksets = linkSets
        newLinksets[currentLinkset].windows.push({
            name: `window ${newLinksets[currentLinkset].windows.length}`,
            links: [
            ]
        });

        setLinksets(newLinksets)

        setTrigger(currentTrigger+1)

        updateLinkset(newLinksets);
    }

    const addLink = (windowIndex, url) => {
        let newLinksets = linkSets
        let links = newLinksets[currentLinkset].windows[windowIndex].links.push(url);

        linkSets = newLinksets;
        setLinksets(linkSets)


        setTrigger(currentTrigger+1)

        updateLinkset(linkSets);
    }

    const updateLinksetName = (linkSetIndex, newLinksetName) => {
        const newLinksets = linkSets.map((linkSet, index) => { if (index === linkSetIndex) linkSet.name = newLinksetName; return linkSet })
        
        setLinksets(newLinksets)

        updateLinkset(newLinksets);
    }

    const toggleEditLinkSet = (linkSetIndex, newLinksetName) => {
        if (editLinksetName) updateLinksetName(linkSetIndex, newLinksetName)
        setEditLinksetName(!editLinksetName)
    }

    const deleteLinkSet = (linkSetIndex) => {
        if (linkSetIndex <= 0) return false
        const newLinksets = linkSets.filter((linkSet, index) => { return index !== linkSetIndex })
        
        setLinksets(newLinksets)

        setCurrentLinkset(0)

        updateLinkset(newLinksets);
    }

    const deleteLink = (windowIndex, linkIndex) => {
        const newLinksets = linkSets

        let links = newLinksets[currentLinkset].windows[windowIndex].links;
        
        //set links without new one
        
        newLinksets[currentLinkset].windows[windowIndex].links = links.filter((link, index) => { return index !== linkIndex })
        
        
        setLinksets(newLinksets)

        setTrigger(currentTrigger+1)

        updateLinkset(newLinksets);
    }

    const editWindowName = (windowIndex, newName) => {

        // get window
        const newLinksets = linkSets
        let windows = newLinksets[currentLinkset].windows;
        
        //set links without new one
        
        newLinksets[currentLinkset].windows = windows.map((window, index) => { if ( index === windowIndex ) { window.name = newName } return window  })
        
        setTrigger(currentTrigger+1)

        setLinksets(newLinksets)

        updateLinkset(newLinksets);
    }

    const deleteWindow = (windowIndex) => {
        
        if (windowIndex <= 0) return false

        const newLinksets = linkSets
        let windows = newLinksets[currentLinkset].windows;
        
        //set links without new one
        
        newLinksets[currentLinkset].windows = windows.filter((window, index) => { return index !== windowIndex })
        
        setTrigger(currentTrigger+1)

        setLinksets(newLinksets)

        updateLinkset(newLinksets);
    }

    const getCurrentLinkset = (currentLinkset) => {
        return linkSets[currentLinkset].windows
    }

    const newLinkset = () => {
        const index = linkSets.length
        const newLinkset ={
            name: `New linkset ${linkSets.length}`,
            windows: [
                {
                    name: 'first',
                    links: [
                        "https://google.de"
                    ]
                }
            ]
        }
        setLinksets(prevState => [...prevState, newLinkset])
        console.log(linkSets)
        updateLinkset(linkSets)
        openModal(index)
    }

    const LinkSetTitle = () => {
        if (editLinksetName) return (
        <div><input onKeyPress={event => {
                if (event.key === 'Enter') {
                    toggleEditLinkSet (currentLinkset, event.target.value)
                }
            }}
            onChange={(event) => { newName = event.target.value }}
            className="bg-white text-sm text-blue-500 p-1 pl-2 cursor-text focus:outline-none focus:shadow-outline border border-gray-300 rounded-lg block appearance-none leading-normal mr-6" type="email" placeholder="Linkset ..." defaultValue={linkSets[currentLinkset].name}></input></div>)
            
            return <p>Edit {linkSets[currentLinkset].name}</p>
    }

    return (
        <div className="bg-gray-200 p-8">
            <h1 className="text-left text-2xl mb-4">Linksets</h1>
            <p className="text-base text-gray-600 leading-normal">You have to accept popups for this website first <a className="no-underline hover:underline text-blue-500 text-lg" href="https://support.google.com/chrome/answer/95472?co=GENIE.Platform%3DAndroid&hl=en" target="_blank">click here for a howto guide</a></p>
            {/* <button onClick={() => this.openTabs({closeWindows: false} ) } class="content-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-4">Open my tabs :*</button>
            <button onClick={() => this.openTabs({closeWindows: true} ) } class="content-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-4">Open my tabs and close old window:*</button>
            */}
            <div className="linkset-actions w-full flex justify-end cursor-pointer">
                <div class="flex justify-between">
                    <button onClick={newLinkset} className="selectable cursor-pointer content-center bg-green-400 hover:bg-green-500 text-white font-bold py-2 px-2 rounded my-4 flex justify-between">
                        <div>
                            <FontAwesomeIcon className="text-white pt-1" size="lg" icon={faPlusSquare} />
                        </div>
                        <p className="pl-2 mb-0">create linkset</p>
                    </button>
                    <button onClick={toggleSetChromeExtensionIdModal} className="ml-2 selectable cursor-pointer content-center bg-green-400 hover:bg-green-500 text-white font-bold py-2 px-2 rounded my-4 flex justify-between">
                        <div>
                            <FontAwesomeIcon className="text-white pt-1" size="lg" icon={faChrome} />
                        </div>
                        <p className="pl-2 mb-0">set chrome extension</p>
                    </button>
                </div>
            </div>
            <ul className="selectable linksets-list mt-4">
                {linkSets.map((set, index) => {
                    return <li className="mb-2 bg-blue-100 p-2 shadow-md flex space-between justify-between" key={index}>
                        <p className="p-2 mb-0">{set.name}</p>
                        <div className="flex">
                            <div onClick={() => openTabs(set, {closeWindows: false})} className="selectable p-2 text-gray-600 hover:text-blue-800 cursor-pointer" title="open linkset"><FontAwesomeIcon icon={faExternalLinkAlt} /></div>
                            <div  onClick={() => openModal(index)} className="selectable p-2 text-gray-600 hover:text-blue-800 cursor-pointer" title="edit linkset"><FontAwesomeIcon icon={faEdit} /></div>
                            <div onClick={() => deleteLinkSet(index)} className="selectable p-2 text-gray-600 hover:text-red-600 cursor-pointer" title="delete linkset"><FontAwesomeIcon icon={faTrash} /></div>
                        </div>
                    </li>
                })}
            </ul>
            {/* modal */}
            <div>
                <SetExtensionId toggleSetChromeExtensionIdModal={toggleSetChromeExtensionIdModal} visible={chromeExtensionIdModal} />
                <Modal
                    isOpen={modalIsOpen}
                    onAfterOpen={afterOpenModal}
                    onRequestClose={closeModal}
                    contentLabel="From subreddit NatureIsFuckingLit"
                >
                    <div className="modal-header min-w-full flex justify-between bg-green-400 text-white p-4">
                        <div className="flex">
                            <LinkSetTitle />
                            <div  onClick={() => toggleEditLinkSet(currentLinkset, newName)} className="selectable text-gray-600 hover:text-blue-800 cursor-pointer pl-2" title="edit linkset" ><FontAwesomeIcon  size="lg" icon={editLinksetName ? faSave : faEdit} /></div>
                        </div>
                        <div>
                            <FontAwesomeIcon className="cursor-pointer text-blue-900 hover:text-red-600" size="lg" icon={faWindowClose} onClick={closeModal} />
                        </div>
                    </div>
                    <ul className="modal-body bg-gray-100 p-6">
                        <div className="modal-actions flex justify-end pt-4 pl-4">
                            {/* new link */}
                            <button onClick={() => openTabs(linkSets[currentLinkset], {closeWindows: false})} title="open linkset" className="mr-2 content-center bg-indigo-500 hover:bg-indigo-400 text-white font-bold py-2 px-2 rounded my-4 flex justify-between">
                                <div>
                                    <FontAwesomeIcon className="cursor-pointer text-white pt-1" size="lg" icon={faExternalLinkAlt} />
                                </div>
                                <p className="pl-2 mb-0">open linkset</p>
                            </button>
                            <button onClick={addWindow} className="content-center bg-green-400 hover:bg-green-500 text-white font-bold py-2 px-2 rounded my-4 flex justify-between">
                                <div>
                                    <FontAwesomeIcon className="cursor-pointer text-white pt-1" size="lg" icon={faPlusSquare} />
                                </div>
                                <p className="pl-2 mb-0">new window</p>
                            </button>
                        </div>
                        <MuuriComponent
                            {...boardOptions}
                        >
                            {getCurrentLinkset(currentLinkset).map ((window, index) => {
                                const {name, links} = window;
                                return <Window
                                    currentTrigger={currentTrigger}
                                    key={index}
                                    index={index}
                                    name={name}
                                    links={links}
                                    editWindowName={(windowIndex, newName) => editWindowName(windowIndex, newName)}
                                    deleteWindow={(windowIndex) => deleteWindow(windowIndex)}
                                    deleteLink={(windowIndex, linkIndex) => deleteLink(windowIndex, linkIndex)}
                                    addNewLink={(url) => addLink(index, url)} />
                            })}
                        </MuuriComponent>
                    </ul>
                </Modal>
            </div>
        </div>
    );
}

export default WidgetLinkSets;
