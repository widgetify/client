import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faExternalLinkAlt, faTrash} from "@fortawesome/free-solid-svg-icons";
import Truncate from 'react-truncate';
import { useDraggable, useRefresh } from "muuri-react";

const LinkItem = React.memo(({ id, name, deleteLink, currentTrigger }) => {

    const setDraggable = useDraggable();

    // Disable dragging for this item.
    setDraggable(false);

    useRefresh([currentTrigger]);

    return (
        <li className={'link-item rounded-lg bg-gray-100 shadow-md flex justify-start'}>
            <div className="link-move bg-green-400 p-2">
            </div>
            <div className="flex w-full pr-4 justify-between">
                <div className="pt-5 pl-2 text-sm text-blue-500" title={name}>
                    <Truncate width={"120"} ellipsis={<span>...</span>}>
                        <p className="align-middle" title={name}>{name}</p>
                    </Truncate>
                </div>
                <div className="flex align-right">
                    <div className="pt-4 text-gray-600 hover:text-blue-800 cursor-pointer" title="open url"><FontAwesomeIcon icon={faExternalLinkAlt} /></div>
                    <div className="pt-4 pl-4 text-gray-600 hover:text-blue-800 cursor-pointer" title="edit url"><FontAwesomeIcon icon={faEdit} /></div>
                    <div onClick={deleteLink} className="pt-4 pl-4 text-gray-600 hover:text-red-600 cursor-pointer" title="delete url"><FontAwesomeIcon icon={faTrash} /></div>
                </div>
            </div>
        </li>
    );
});

export default LinkItem;