import {MuuriComponent} from "muuri-react";
import React, { useState } from "react";
import LinkItem from './LinkItem'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExternalLinkAlt, faEdit, faTrash, faWindowClose, faSave } from "@fortawesome/free-solid-svg-icons";
import { useRefresh, useDraggable } from "muuri-react";

const columnOptions = {
    // Enable to send the items in
    // the grids with the following groupId.
    dragSort: { groupId: "Window" },
    groupIds: ["Window"],
    containerClass: "board-column-content",
    dragEnabled: true,
    dragSortHeuristics: {
        sortInterval: 0
    },
    dragPlaceholder: {
        enabled: true,
        createElement: function(item) {
            return item.getElement().cloneNode(true);
        }
    },
    dragContainer: document.body
};

const onSend = () => {};

const Window = ({key, index, name, links, addNewLink, deleteLink, editWindowName, deleteWindow, currentTrigger}) => {

    const [editName, setEditName] = useState(false);

    const setDraggable = useDraggable();

    // Disable dragging for this item.
    setDraggable(false);

    useRefresh([currentTrigger]);

    let newName = name;

    const toggleEditName = () => {
        if (editName) editWindowName(index, newName); 
        setEditName(!editName)
    }

    const Title = () => {
        if (editName) { return <div><input onKeyPress={event => {
            if (event.key === 'Enter') {
                toggleEditName (event.target.value)
            }
        }}

        onChange={(event) => { newName = event.target.value }}
        
        className="bg-white text-sm text-blue-500 p-1 pl-2 cursor-text focus:outline-none focus:shadow-outline border border-gray-300 rounded-lg block appearance-none leading-normal mr-6" type="email" placeholder="Window ..." defaultValue={name}></input></div>
        }
        return <p className={""}> {name} </p>
    }

    const EditIcon  = () => {
        if (editName) { 
            return <FontAwesomeIcon icon={faSave} />
        }
        return <FontAwesomeIcon icon={faEdit} />
    }


    return (<li className={"window m-2 rounded-lg shadow-lg bg-white"}>
            <div>
                <div className="window-header py-4 pr-4 pl-2 shadow-sm text-white bg-indigo-500 flex justify-between">
                    <Title />
                    <div className="flex justify-between">
                        <div onClick={() => toggleEditName(newName)} className="selectable text-gray-100 hover:text-red-400 cursor-pointer pr-4" title="edit url"><EditIcon /></div>
                        <div  onClick={() => deleteWindow(index)} className="selectable text-gray-100 hover:text-red-400 cursor-pointer" title="delete linkset"><FontAwesomeIcon icon={faTrash} /></div>
                    </div>
                </div>
                <ul className={"flex flex-col mt-4 pl-2 justify-spacearound"}>
                    { /* link card */}
                    <MuuriComponent
                        dragEnabled
                        {...columnOptions}
                        onSend={onSend}
                        dragStartPredicate={function (item, hammerEvent) {
                            // Don't drag if target is button
                            if (hammerEvent.target.matches("button, select, input, a, .selectable,.selectable *, .react-resizable-handle ")) {
                                return false;
                            }
                            if (hammerEvent.target.matches(".link-move")) {
                                return true;
                            }
                            return false;
                        }}
                        id={name}
                    >
                        {links.map(function (link, linkIndex) {
                            return <li className={'urlItem mb-2'}>
                                        <LinkItem currentTrigger deleteLink={() => deleteLink(index, linkIndex)} name={link} id={index}  style={{width: '100%'}}/>
                                    </li>
                        })}
                    </MuuriComponent>
                    { /* end link card */}
                </ul>
                <div className="p-2">
                    <input onKeyPress={event => {
                if (event.key === 'Enter') {
                    addNewLink(event.target.value);
                    event.target.value = ""
                }
              }} className="bg-white text-sm text-blue-500 text- cursor-text focus:outline-none focus:shadow-outline border border-gray-300 rounded-lg py-2 px-4 block appearance-none leading-normal w-full" type="email" placeholder="http://example.de"></input>
                </div>            
            </div>
        </li>)
}

export default Window;
