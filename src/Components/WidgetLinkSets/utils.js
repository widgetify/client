/*global chrome*/
const chromeExtensionId = "gcbadifihipmpbhjabicaohbmopmieec"

const getChromeExtensionId = () => {
    return localStorage.getItem('widget-linkset-chromeId')
} 

const openTabs = (set, options) => {
    let windows = set.windows
    chrome.runtime.sendMessage(getChromeExtensionId(), { windows, options}, function (response) {})
}

export { openTabs }