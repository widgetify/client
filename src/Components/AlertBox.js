import React from 'react';

const AlertBox = (props) => {
    return (
            <div role="alert">
              <div className="bg-red-300 text-white font-bold rounded-t px-4 py-2">
                !
              </div>
              <div className="border border-t-0 border-red-200 rounded-b bg-red-100 px-4 py-3 text-red-600">
                {props.children}
              </div>
            </div>
            );
};

export default AlertBox;



