import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import SubtleButton from  './SubtleButton'
import { getUser } from '../queries/queries';
import { useQuery } from '@apollo/react-hooks';
import jwt_decode from 'jwt-decode'

//fa
import { faCog, faSignInAlt, faUser, faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

//antdesign
import { PageHeader, Avatar } from 'antd';
import 'antd/dist/antd.css'
import '../styles/logged.scss'


const Logged = () => {

    let userId = ""
    if (localStorage.usertoken){
        userId = jwt_decode(localStorage.usertoken)._id
    }

    const [confirmed, setconfirmed] = useState(false);
    const [user, setuser] = useState({name: 'John Doe'});
    const { loading, error, data } = useQuery(getUser, {variables: { 
            _id: userId 
        },
    });

    useEffect(() => {
        if(data && data.userById) {
            setconfirmed(data.userById.confirmed);
            setuser(data.userById);
        }
        console.log('data:', data)
    }, [data])

    useEffect(() => {
        console.log('confirmed:', confirmed)
    }, [confirmed])


    function logout(){
        localStorage.removeItem('usertoken');
        window.location.reload(true);
    }
    const notlogged = (
        <PageHeader
            className="site-page-header"
            extra={[
                <Link to = "/login" className="mr-4" alt={"signup"} style={{cursor: 'pointer'}}><FontAwesomeIcon  size="lg" icon={faSignInAlt} /> Login</Link>,
                <Link to = "/signup" alt="login" style={{cursor: 'pointer'}}><FontAwesomeIcon size="lg" icon={faUser} /> Register</Link>
            ]}
        >
            
        </PageHeader>
    )
    const logged = (

        <div className="site-page-header-ghost-wrapper bg-white">
            <PageHeader
                className="site-page-header"
                subTitle="Welcome to Widgetify"
                avatar={{icon: <div>{user.name.substr(0, 2)}</div>, style: {cursor:'pointer'}}}
                extra={[
                    <button className="mr-4" alt={"settings"} style={{cursor: 'pointer'}}><FontAwesomeIcon  size="lg" icon={faCog} /> Settings</button>,
                    <button  onClick = {logout} alt="Sign out" style={{cursor: 'pointer'}}><FontAwesomeIcon size="lg" icon={faSignOutAlt} /> Sign out</button>
                  ]}
            >
                 
            </PageHeader>
           
        </div>
    )
    return (
            <div className="">
                <div>{localStorage.usertoken && confirmed == true ? logged : notlogged}</div> 
            </div>
    );
}
 
export default Logged;