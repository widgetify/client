import React, { Component } from 'react';

class CheckboxSwitcher extends Component {


    render(props) { 
        return (
            <div class="selectable" onClick={this.props.switchClick}>
                {this.props.switch_status
                    ? <span class="border rounded-full bg-white border-grey-600 flex items-center cursor-pointer w-12 bg-green-500 justify-end">
                    <span class="rounded-full border w-6 h-6 border-grey shadow-inner bg-white shadow">
                    </span>
                    </span>
                    : <span class="border rounded-full bg-white border-grey-600 flex items-center cursor-pointer w-12 justify-start">
                    <span class="rounded-full border w-6 h-6 border-grey-600 shadow-inner bg-white shadow">
                    </span>
                    </span>
                }
            </div>
        );
    }
}
 
export default CheckboxSwitcher;