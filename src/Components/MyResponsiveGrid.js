//https://github.com/STRML/react-grid-layout/blob/master/test/examples/1-basic.jsx
import React from "react";
import RGL, { WidthProvider } from "react-grid-layout";
import Calculator from './Calculator'
import Natureify from './Natureify'
import TimeZones from './TimeZones'
import SubtleButton from './SubtleButton'
import Translate from './Translate'
import WidgetTopSites from './WidgetTopSites'
import WidgetWhatToDo from './WidgetWhatToDo'
import _ from "lodash";

const ReactGridLayout = WidthProvider(RGL);
const originalLayout = getFromLS("layout") || [];
const widgetCSS  = "p-3 bg-white shadow-md rounded w-1/5"
const widgetCSS2 = widgetCSS.concat(" cancel-drag")


const widgets = [
  {
    "component":<Calculator></Calculator>,
    "w": 1,
    "h": 2,
    "draggable": true,
    "isResizable": true,
    "static": false,
    "minW": 0,
    "maxW": Infinity,
    "minH": 0,
    "maxH": Infinity
  },
  {
    "component":<Natureify></Natureify>,
    "w": 2,
    "h": 5,
    "draggable": true,
    "isResizable": true,
    "static": false,
    "minW": 0,
    "maxW": Infinity,
    "minH": 0,
    "maxH": Infinity
  },
  {
    "component":<TimeZones></TimeZones>,
    "w": 2,
    "h": 4,
    "draggable": true,
    "isResizable": true,
    "static": false,
    "minW": 0,
    "maxW": Infinity,
    "minH": 0,
    "maxH": Infinity
  },
  {
    "component":<Translate></Translate>,
    "w": 2,
    "h": 4,
    "draggable": true,
    "isResizable": true,
    "static": false,
    "minW": 0,
    "maxW": Infinity,
    "minH": 0,
    "maxH": Infinity
  },
  {
    "component":<WidgetTopSites></WidgetTopSites>,
    "w": 2,
    "h": 4,
    "draggable": true,
    "isResizable": true,
    "static": false,
    "minW": 0,
    "maxW": Infinity,
    "minH": 0,
    "maxH": Infinity
  },
  {
    "component":<WidgetWhatToDo />,
    "w": 2,
    "h": 6,
    "draggable": true,
    "isResizable": true,
    "static": false,
    "minW": 0,
    "maxW": Infinity,
    "minH": 0,
    "maxH": Infinity
  }
]

const widgets2 = [
  {
    "component":<Translate></Translate>,
    "w": 2,
    "h": 4,
    "draggable": true,
    "isResizable": true,
    "static": false,
    "minW": 0,
    "maxW": Infinity,
    "minH": 0,
    "maxH": Infinity
  },
  {
    "component":<WidgetTopSites></WidgetTopSites>,
    "w": 2,
    "h": 4,
    "draggable": true,
    "isResizable": true,
    "static": false,
    "minW": 0,
    "maxW": Infinity,
    "minH": 0,
    "maxH": Infinity
  },
  {
    "component":<WidgetWhatToDo />,
    "w": 2,
    "h": 6,
    "draggable": true,
    "isResizable": true,
    "static": false,
    "minW": 0,
    "maxW": Infinity,
    "minH": 0,
    "maxH": Infinity
  }
]


export default class MyResponsiveGrid extends React.PureComponent {
  static defaultProps = {
    className: "layout",
    cols: 7,
    rowHeight: 50,
    draggableCancel: '.cancel-drag',
    onLayoutChange: function() {}
  };


  constructor(props) {
    super(props);

    this.state = {
      layout: JSON.parse(JSON.stringify(originalLayout)),
      currentSet: 0
    };

    this.onLayoutChange = this.onLayoutChange.bind(this);
    this.resetLayout = this.resetLayout.bind(this);

    
    this.toggleCurrentSet = this.toggleCurrentSet.bind(this);
  }

  resetLayout() {
    this.setState({
      layout: []
    });
  }

  onLayoutChange(layout) {
    /*eslint no-console: 0*/
    saveToLS("layout", layout);
    this.setState({ layout });
    this.props.onLayoutChange(layout); // updates status display
  }
  
  generateWidgets(widgets) {
    var widgetsArray = []
    var prevWidth = 0
    for (var i=0;i<widgets.length;i++){
      widgetsArray.push(
        <div  className={widgets[i].draggable==true?widgetCSS:widgetCSS2} key={i} data-grid={{ w: widgets[i].w, h: widgets[i].h, x: prevWidth, y: 0 }}>
            {widgets[i].component}
        </div>
      )
      prevWidth += widgets[i].w
    }
    return widgetsArray
  }

  toggleCurrentSet () {
    this.setState({currentSet: !this.state.currentSet})
  }


  render() {
    return (
      
      <div className="my-4 mx-8">
       <button onClick={this.toggleCurrentSet} class="mx-auto bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-4">Toggle widget Set</button>
      <SubtleButton onClick={this.resetLayout}>reset layout</SubtleButton>
        <ReactGridLayout
          {...this.props}
          layout={this.state.layout}
          onLayoutChange={this.onLayoutChange}
        >
          {this.state.currentSet ? this.generateWidgets(widgets) : this.generateWidgets(widgets2)}
        </ReactGridLayout>
      </div>
    );
  }
}

function getFromLS(key) {
  let ls = {};
  if (global.localStorage) {
    try {
      ls = JSON.parse(global.localStorage.getItem("rgl-7")) || {};
    } catch (e) {
      /*Ignore*/
    }
  }
  return ls[key];
}

function saveToLS(key, value) {
  if (global.localStorage) {
    global.localStorage.setItem(
      "rgl-7",
      JSON.stringify({
        [key]: value
      })
    );
  }
}




// import React from 'react';
// import { Responsive as ResponsiveGridLayout } from 'react-grid-layout';

// class MyResponsiveGrid extends React.Component {
//   render() {
//     const layout1= [
//       {i: 'a', x: 0, y: 0, w: 1, h: 2, static: true},
//       {i: 'b', x: 1, y: 0, w: 3, h: 2, minW: 2, maxW: 4},
//       {i: 'c', x: 4, y: 0, w: 1, h: 2}
//     ];
//     const layout2 = [
//       {i: 'a', x: 0, y: 0, w: 1, h: 2, static: true},
//       {i: 'b', x: 1, y: 0, w: 3, h: 2, minW: 2, maxW: 4},
//       {i: 'c', x: 4, y: 0, w: 1, h: 2}
//     ];
//     // {lg: layout1, md: layout2, ...}
//     const layouts = {"lg":layout1, "xs":layout2}
//     return (
//       <ResponsiveGridLayout className="layout" layouts={layouts}
//         breakpoints={{lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0}}
//         cols={{lg: 12, md: 10, sm: 6, xs: 4, xxs: 2}}>
//         <div key="1" className="w-1/3 bg-gray-200 rounded">1</div>
//         <div key="2" className="w-1/3 bg-gray-200 rounded">2</div>
//         <div key="3" className="w-1/3 bg-gray-200 rounded">3</div>
//       </ResponsiveGridLayout>
//     )
//   }
// }

// // const Draglist = () => {

// //     return (
// //             <div className="bg-gray-500 rounded m-8 w-1/5">
// //               <ul>
// //                   <li>hi</li>
// //                   <li>ho</li>
// //                   <li>he</li>
// //                   <li>she</li>
// //               </ul>
// //             </div>
// //             );
// // };

// export default MyResponsiveGrid;



// import React from "react";
// import RGL, { WidthProvider } from "react-grid-layout";

// const ReactGridLayout = WidthProvider(RGL);
// const originalLayout = getFromLS("layout") || [];
// /**
//  * This layout demonstrates how to sync to localstorage.
//  */
// export default class LocalStorageLayout extends React.PureComponent {
//   static defaultProps = {
//     className: "layout",
//     cols: 12,
//     rowHeight: 30,
//     onLayoutChange: function() {}
//   };

//   constructor(props) {
//     super(props);

//     this.state = {
//       layout: JSON.parse(JSON.stringify(originalLayout))
//     };

//     this.onLayoutChange = this.onLayoutChange.bind(this);
//     this.resetLayout = this.resetLayout.bind(this);
//   }

//   resetLayout() {
//     this.setState({
//       layout: []
//     });
//   }

//   onLayoutChange(layout) {
//     /*eslint no-console: 0*/
//     saveToLS("layout", layout);
//     this.setState({ layout });
//     this.props.onLayoutChange(layout); // updates status display
//   }

//   render() {
//     return (
//       <div>
//         <button onClick={this.resetLayout}>Reset Layout</button>
//         <ReactGridLayout
//           {...this.props}
//           layout={this.state.layout}
//           onLayoutChange={this.onLayoutChange}
//         >
//           <div key="1" data-grid={{ w: 2, h: 3, x: 0, y: 0 }}>
//             <span className="text">1</span>
//           </div>
//           <div key="2" data-grid={{ w: 2, h: 3, x: 2, y: 0 }}>
//             <span className="text">2</span>
//           </div>
//           <div key="3" data-grid={{ w: 2, h: 3, x: 4, y: 0 }}>
//             <span className="text">3</span>
//           </div>
//           <div key="4" data-grid={{ w: 2, h: 3, x: 6, y: 0 }}>
//             <span className="text">4</span>
//           </div>
//           <div key="5" data-grid={{ w: 2, h: 3, x: 8, y: 0 }}>
//             <span className="text">5</span>
//           </div>
//         </ReactGridLayout>
//       </div>
//     );
//   }
// }

// function getFromLS(key) {
//   let ls = {};
//   if (global.localStorage) {
//     try {
//       ls = JSON.parse(global.localStorage.getItem("rgl-7")) || {};
//     } catch (e) {
//       /*Ignore*/
//     }
//   }
//   return ls[key];
// }

// function saveToLS(key, value) {
//   if (global.localStorage) {
//     global.localStorage.setItem(
//       "rgl-7",
//       JSON.stringify({
//         [key]: value
//       })
//     );
//   }
// }
