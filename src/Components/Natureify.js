import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Modal from 'react-modal';
import SubtleMessage from './SubtleMessge';

function Natureify() {
 //_-_-_-_-_-_-_-_-_-_-MODAL_-_-_-_-_-_-_-_-_-_-_-_-_-_-
const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
    height: '90%',
    width: '60%'
  }
};

// var subtitle;
const [modalIsOpen,setIsOpen] = useState(false);
function openModal() {
  setIsOpen(true);
}

function afterOpenModal() {
  // references are now sync'd and can be accessed.
  // subtitle.style.color = 'bg-color';
}

function closeModal(){
  setIsOpen(false);
}
 //_-_-_-_-_-_-_-_-_-_-MODAL-ENDS_-_-_-_-_-_-_-_-_-_-_-_-_-_-

    const [data, setData] = useState([]);
    const [photoArray, setPhotoArray] = useState([]);
    const [nameArray, setNameArray] = useState([]);
    const [photoNum, setPhotoNum] = useState(0);
    const [photoName, setPhotoName] = useState("");
    useEffect( () => {
      const fetchData = async () => {
        const result = await axios(
          {
              url: 'http://' + process.env.REACT_APP_NODE_AUTH_URL + '/bucketoauth',
              // url: 'http://' + process.env.REACT_APP_REQUEST_OAUTH_TOKEN_URL,
              method: 'GET',
              },
          );
        const result2 = await axios(
          {
              url: 'https://storage.googleapis.com/storage/v1/b/bucket-for-current-photos/o',
              method: 'GET',
              headers: {'Authorization': 'Bearer '+ result.data.access_token},
          },
        )
          setData(result2.data.items)
          
        }
      fetchData();
    }, []);

    useEffect( () => {
      if (data) {
        setNameArray(data.map(photo => photo.name.split('_').join(' ').substr(0, photo.name.length-4)))
        setPhotoArray(data.map(photo => photo.name))
      }
    },[data])
    
    useEffect( () => {
      setPhotoName(nameArray[0])
    },[photoArray])


    function next (){
      if (photoNum<photoArray.length-1){
        setPhotoNum(photoNum+1)
      } else {
        setPhotoNum(0)
      }
      setPhotoName(nameArray[photoNum+1])
    }

    function prev (){
      if (photoNum>0){
        setPhotoNum(photoNum-1)
      } else {
        setPhotoNum(photoArray.length-1)
      }
      setPhotoName(nameArray[photoNum-1])
    }


  var imgUrl = 'https://storage.googleapis.com/bucket-for-current-photos/'+photoArray[photoNum]
  
    
  return (
    <div className='bg-cover' style={{backgroundImage: 'url(' + imgUrl + ')', height:'100%'}}>
{/* _-_-_-_-_-_-_-_MODAL-_-_-_-_-_-_-_-_-_-_- */}
      <button className="hover:text-blue-700 text-gray-400 m-2 absolute z-50" onClick={prev}> prev </button>
      <button className="hover:text-blue-700 text-gray-400 m-2 ml-12 absolute z-40" onClick={next}> next </button>
      <button onClick={openModal} className='w-11/12 h-full absolute block z-10'></button>
      <div>
        <Modal
          isOpen={modalIsOpen}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="From subreddit NatureIsFuckingLit"
        >
          <button className='block' onClick={closeModal}>X</button>
          <SubtleMessage className='break-words w-full'>{photoName}</SubtleMessage>
          <button className="hover:text-blue-700 text-gray-400 m-2 absolute z-50" onClick={prev}> prev </button>
          <button className="hover:text-blue-700 text-gray-400 m-2 ml-12 absolute z-40" onClick={next}> next </button>
          <img className='h-full' src={imgUrl} alt=""/>
        </Modal>
      </div>
    </div>
  );
}
export default Natureify;