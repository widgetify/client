// Provided by this package.
import React, { useState, useEffect } from "react";
import ReactDom from "react-dom";
import { MuuriComponent, useDrag } from "muuri-react";
import ResizableWrapper from "./ResizableWrapper"
// Widgets
import Calculator from './Calculator'
import Natureify from './Natureify'
import TimeZones from './TimeZones'
import SubtleButton from './SubtleButton'
import Translate from './Translate'
import WidgetTopSites from './WidgetTopSites'
import WidgetWhatToDo from './WidgetWhatToDo'
import '../styles/grid.css'
import WidgetLinkSets from "./WidgetLinkSets";

/*
const Item = ({ id, key, component, height, width }) => {
    const isDragging = useDrag();
    return (
            <div className={`item m-6 max-w-sm rounded overflow-hidden shadow-lg bg-gray-200`} style={{height: height, width: width}}>
                <div className="item-content" style={{height: height, width: width}}>
                    {isDragging ? component : component}
                </div>
            </div>
    );
  }; */

  const Item = ResizableWrapper(
    ({ component}) => (
      component
    ),
    {
      width: 400,
      height: 200
    }
  );



const NewGrid = () => {

    const [items, setItems] = useState([]);

    useEffect(() => {
      setItems([{
        id: '1',
        component: <WidgetLinkSets />,
        minWidth: 400,
        minHeight: 400,
        width: getWidgetSize(1).width,
        height: getWidgetSize(1).height
    },

    {
        id: '2',
        component: <Natureify />,
        minWidth: 400,
        minHeight: 400,
        width: getWidgetSize(2).width,
        height: getWidgetSize(2).height
    },
    {
        id: '3',
        component: <Calculator />,
        minWidth: 200,
        minHeight: 400,
        width: getWidgetSize(3).width,
        height: getWidgetSize(3).height
    },
    {
        id: '4',
        component: <WidgetWhatToDo />,
        minWidth: 400,
        minHeight: 400,
        width: getWidgetSize(4).width,
        height: getWidgetSize(4).height
    },
    {
        id: '5',
        component: <TimeZones />,
        minWidth: 400,
        minHeight: 400,
        width: getWidgetSize(5).width,
        height: getWidgetSize(5).height
    },
    {
        id: '6',
        component: <Translate />,
        minWidth: 400,
        minHeight: 400,
        width: getWidgetSize(6).width,
        height: getWidgetSize(6).height
        
    },
    {
        id: '7',
        component: <WidgetTopSites />,
        minWidth: 400,
        minHeight: 400,
        width: getWidgetSize(7).width,
        height: getWidgetSize(7).height
    }]);
    }, [])

    const getWidgetSize = (id) =>  {
      console.log(`widget-${id}`, localStorage.getItem(`widget-${id}`))
      return localStorage.getItem(`widget-${id}`) ? JSON.parse(localStorage.getItem(`widget-${id}`)) : {width: 400, height: 200}
    }

    // Items to components.
    const children = items.map((item) => <Item className="m-6 max-w-sm rounded overflow-hidden shadow-lg bg-gray-200" key={item.id} color="red" {...item}/>);
  
    // The 'setItems' call will re-render the component with different items.
    const add = (id) => setItems(items.concat({id}));
    const remove = (id) => setItems(items.filter((item) => item.id !== id));

    const options = {
        dragSortHeuristics: {
          sortInterval: 70
        },
        dragContainer: document.body,
        // The placeholder of an item that is being dragged.
        dragPlaceholder: {
          enabled: true,
          createElement: function(item) {
            // The element will have the Css class ".muuri-item-placeholder".
            return item.getElement().cloneNode(true);
          }
        }
      };

    function childOf(c,p){while((c=c.parentNode)&&c!==p);return !!c}

    return <MuuriComponent
        dragEnabled
        {...options}
        dragStartPredicate={function (item, hammerEvent) {
            // Don't drag if target is button
            if (hammerEvent.target.matches("button, select, input, a, .selectable,.selectable *, .react-resizable-handle ")) {
                return false;
            }
            // handle Drag
            return true;
        }}
    >
        {children}
    </MuuriComponent>;
};

export default NewGrid;