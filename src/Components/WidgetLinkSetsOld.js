/*global chrome*/

import React, {Component, useState} from 'react';
import { MuuriComponent, useDrag } from "muuri-react";
import { faExternalLinkAlt, faEdit, faTrash, faWindowClose } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Modal from 'react-modal';
import '../styles/modal.scss'
import '../styles/linkset.scss'

const columnOptions = {
    // Enable to send the items in
    // the grids with the following groupId.
    dragSort: { groupId: "NOTES" },
    groupIds: ["NOTES"],
    containerClass: "board-column-content",
    dragEnabled: true,
    dragFixed: true,
    dragSortHeuristics: {
        sortInterval: 0
    },
    dragContainer: document.body
};

// Board static options.
const boardOptions = {
    layoutDuration: 400,
    dragEnabled: true,
    dragStartPredicate: {
        handle: ".window-header"
    },
    containerClass: "flex"
};

const options = {
    dragSort: { groupId: "NOTES" },
    groupIds: ["NOTES"],
    containerClass: "board-column-content",
    dragEnabled: true,
    dragFixed: true,
    dragSortHeuristics: {
        sortInterval: 0
    },
    dragContainer: document.body
};


const onSend = () => {};

const Item = React.memo(({ id, name }) => {

    return (
        <li className={'mb-2 bg-blue-100 p-2 shadow-md flex space-between justify-between'}>
            <p className="p-2">{name}</p>
            <div className="flex">
                <div
                    className="selectable p-2 text-gray-600 hover:text-blue-800 cursor-pointer"
                    title="open linkset"><FontAwesomeIcon icon={faExternalLinkAlt}/></div>
                <div
                    className="selectable p-2 text-gray-600 hover:text-blue-800 cursor-pointer"
                    title="edit linkset"><FontAwesomeIcon icon={faEdit}/></div>
                <div className="selectable p-2 text-gray-600 hover:text-red-600 cursor-pointer"
                     title="delete linkset"><FontAwesomeIcon icon={faTrash}/></div>
            </div>
        </li>
    );
});

class WidgetLinkSets extends Component {


    openModal = () => {
        this.setState({ modalIsOpen: true })
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        // subtitle.style.color = 'bg-color';
    }

    closeModal = () => {
        this.setState({ modalIsOpen: false })
    }

    state = {
        chromeExtensionId: "gdhfbehjgkhkejomjlfciaieijojccnp",
        modalIsOpen: false,
        linkSets: [
            {
                name: "Client aquisition",
                windows: [
                    {
                        links: [
                            "https://julianbertsch.de",
                            "https://xing.de"
                        ]
                    },
                    {
                        links: [
                            "https://facebook.com"
                        ]
                    }
                ]
            },
            {
                name: "Uni",
                windows: [
                    {
                        links: [
                            "https://app.code.berlin"
                        ]
                    },
                    {
                        links: [
                            "https://facebook.com"
                        ]
                    }
                ]
            }
        ]
    }

    componentDidMount() {
        this.openModal();
    }

    render() {
        return (
            <div class="bg-gray-200 p-8">
                <h1 class="text-left text-2xl mb-4">Linksets</h1>
                <p class="text-base text-gray-600 leading-normal">You have to accept popups for this website first <a class="no-underline hover:underline text-blue-500 text-lg" href="https://support.google.com/chrome/answer/95472?co=GENIE.Platform%3DAndroid&hl=en" target="_blank">click here for a howto guide</a></p>
                {/* <button onClick={() => this.openTabs({closeWindows: false} ) } class="content-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-4">Open my tabs :*</button>
                <button onClick={() => this.openTabs({closeWindows: true} ) } class="content-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-4">Open my tabs and close old window:*</button>
                */}
                <ul className="selectable mt-4">
                    {this.state.linkSets.map((set, index) => {
                        return <li class="mb-2 bg-blue-100 p-2 shadow-md flex space-between justify-between" key={index}>
                            <p className="p-2">{set.name}</p>
                            <div className="flex">
                                <div onClick={() => this.openTabs(set, {closeWindows: false})} className="p-2 text-gray-600 hover:text-blue-800 cursor-pointer" title="open linkset"><FontAwesomeIcon icon={faExternalLinkAlt} /></div>
                                <div  onClick={this.openModal} className="p-2 text-gray-600 hover:text-blue-800 cursor-pointer" title="edit linkset"><FontAwesomeIcon icon={faEdit} /></div>
                                <div className="p-2 text-gray-600 hover:text-red-600 cursor-pointer" title="delete linkset"><FontAwesomeIcon icon={faTrash} /></div>
                            </div>
                        </li>
                    })}
                </ul>
                {/* modal */}
                <div className="">
                    <Modal
                        isOpen={this.state.modalIsOpen}
                        onAfterOpen={this.afterOpenModal}
                        onRequestClose={this.closeModal}
                        contentLabel="From subreddit NatureIsFuckingLit"
                        className=""
                    >
                        <div class="flex justify-end">
                            <FontAwesomeIcon className="cursor-pointer text-gray-600 hover:text-red-600" size="lg" icon={faWindowClose} onClick={this.closeModal} />
                        </div>
                        <ul>
                        <MuuriComponent {...boardOptions} className={""}>
                                <li className={"window m-2 shadow-lg p-4 bg-white"}>
                                    <ul className={"flex flex-col"}>
                                    <p className={"window-header"}> Window 1 </p>
                                        { /* link card */}
                                        <MuuriComponent
                                            dragEnabled
                                            {...columnOptions}
                                            onSend={onSend}
                                            dragStartPredicate={function (item, hammerEvent) {
                                                // Don't drag if target is button
                                                if (hammerEvent.target.matches("button, select, input, a, .selectable,.selectable *, .react-resizable-handle ")) {
                                                    return false;
                                                }
                                                // handle Drag
                                                return true;
                                            }}
                                            id={"Window1"}
                                        >
                                            <li className={'urlItem m-2'}>
                                                <Item name={"first"} id={"1"}  style={{width: '100%'}}/>
                                            </li>
                                            <li className={'urlItem m-2'}>
                                                <Item name={"second"} id={"1"} style={{width: '100%'}}/>
                                            </li>
                                        </MuuriComponent>
                                        { /* end link card */}
                                    </ul>
                                </li>
                                <li className={"window m-2 flex shadow-lg p-4 bg-white"}>
                                    <p className={"window-header"}>Window 2</p>
                                    { /* link card */}
                                    <MuuriComponent
                                        dragEnabled
                                        {...columnOptions}
                                        dragStartPredicate={function (item, hammerEvent) {
                                            // Don't drag if target is button
                                            if (hammerEvent.target.matches("button, select, input, a, .selectable,.selectable *, .react-resizable-handle ")) {
                                                return false;
                                            }
                                            // handle Drag
                                            return true;
                                        }}
                                        id={"Window1"}
                                    >
                                        <li className={'urlItem m-2'}>
                                            <Item name={"third"} id={"3"}  style={{width: '100%'}}/>
                                        </li>
                                        <li className={'urlItem m-2'}>
                                            <Item name={"fourth"} id={"4"} style={{width: '100%'}}/>
                                        </li>
                                    </MuuriComponent>
                                    { /* end link card */}
                                </li>
                        </MuuriComponent>
                        </ul>
                    </Modal>
                </div>
            </div>
        );
    }

    openTabs = (set, options) => {
        let linkUrls = set.windows[0].links
        chrome.runtime.sendMessage(this.state.chromeExtensionId, { urls: linkUrls, options}, function (response) {})
    }
}

export default WidgetLinkSets;