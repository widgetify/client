import React from 'react';

const Button = (props) => {

    return (
            <button onClick={props.onClick} type={props.type} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 m-2 rounded float-right">
            {props.children}
            </button>
            );
};

export default Button;
