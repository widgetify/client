import React from 'react';

const SubtleMessge = (props) => {
    return (
            <p id={props.id} className="text-sm text-gray-500 italic">
            {props.children}
            </p>
            );
};

export default SubtleMessge;
