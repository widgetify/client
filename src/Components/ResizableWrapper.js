import React, { useRef } from "react";
import { useRefresh } from "muuri-react";
import { ResizableBox } from "react-resizable";
import { debounce } from "underscore";

export default (Component, { width, height }) => {
    
    // Return the wrapped resizable component.
    return function WrappedComponent(props) {
      const { width, height, minWidth, minHeight } = props;
      // Muuri-react provides all the tools to manage scaling.
      // You can implement it however you want.
      const ref = useRef();
      const refresh = useRefresh();
      // Get the best performance with debouncing.
      // It is not mandatory to use.
      const refreshWithdebounce = debounce(
        () => requestAnimationFrame(refresh),
        50
      );

      const saveResize = (props, width, height) => {
        const { id, minWidth, minHeight } = props;
        localStorage.setItem(`widget-${id}`, JSON.stringify({width, height}))
      }
  
      return (
        <div
          ref={ref}
          className={`item shadow-lg bg-gray-200 m-6 overflow-hidden`}
          style={{ width: `${width}px`, height: `${height}px` }}
        >
          <div className="item-content">
            {/* React-resizable is used to handle the resizing. */}
            <ResizableBox
              width={width}
              height={height}
              minConstraints={[minWidth, minHeight]}
              onResize={(_, { size }) => {
                const {width, height} = size
                ref.current.style.width = width + "px";
                ref.current.style.height = height + "px";
                saveResize(props, width, height)
                console.log('size', props)
                refreshWithdebounce();
              }}
            >
              <Component {...props} />
            </ResizableBox>
          </div>
        </div>
      );
    };
  };