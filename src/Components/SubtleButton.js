import React from 'react';

const SubtleButton = (props) => {

    return (
            <button onClick={props.onClick} type={props.type} className="hover:text-blue-700 text-gray-400 font-bold m-2">
            {props.children}
            </button>
            );
};

export default SubtleButton;
