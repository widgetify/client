import React, {useState, useEffect} from 'react';
import axios from 'axios';
import InputField from './InputField'

const Calculator = () => {


    const [operation, setOperation]         = useState("")
    const [jsonOperation, setJsonOperation] = useState(null)
    const [result, setResult]               = useState("")

    const onChange = (e) => {
        setOperation(e.target.value)
    }

    useEffect(() => {
        var operators = ["+", "-", "*", "/"]
        var indexOfOperator;
        let operationDict = {
            "num1":"",
            "op":"",
            "num2":""
        }

        for (var i=0; i<operators.length; i++){
            if (operation.indexOf(operators[i])!=-1){
                indexOfOperator = operation.indexOf(operators[i])
                break
            }
        }

        let operatorCount = operation.split(/[+,\-,*,\/]/).length - 1
        
        if (operatorCount>1){
            console.log("Error, too many operators")
        }

        const operationArray = operation.split(/[+,\-,*,\/]/)
        operationArray.splice(1, 0, operation[indexOfOperator])

        var i = 0 
        for (const key in operationDict) {
            operationDict[key] = operationArray[i]
            i++
          }
        setJsonOperation(JSON.stringify(operationDict))
      });

    const onKeyDown = e => {
      if (e.key === 'Enter') {
        e.preventDefault();
        e.stopPropagation();
        onSubmit()
      }
    }
    const onSubmit = () => {
        axios.post(process.env.REACT_APP_GO_CALCULATOR, jsonOperation)
          .then(response => {
            if (response.data.error){
              console.log("Error:", response.data.error)
            }
            else{
              console.log("response:",response.data)
              setResult(response.data)
            }
          })
          .catch(error => {
            console.log(error)
          }
        )
      }

    return (
    <div>
        <form onKeyDown={onKeyDown}>
            <div className='text-blue-300 font-semibold'>
              <InputField className="mx-4" label="operation" name='operation' placeholder='math operation' type="text" value={operation} onChange={onChange}></InputField>
            </div>
        </form>
        <p>{result!=null?result:null}</p>
    </div>
            );
};

export default Calculator;
