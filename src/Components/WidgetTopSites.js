import React from 'react';
import SubtleMessge from './SubtleMessge';


const WidgetTopSites = (props) => {

    return (
            <div>
                
                <SubtleMessge id="extension-message">Install <a className="text-blue-500 underline" href="https://chrome.google.com/webstore/detail/widgetify-top-sites/ppehgipichbfakfgfnimomoihngfebgp">Widgetify Top Sites Chrome extension</a> to see your top visited websites</SubtleMessge>
                <ul id="top-sites">
                    
                </ul>
            </div>
            );
};

export default WidgetTopSites;
