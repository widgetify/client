import React from 'react';

const InputField = (props) => {
    return (
                <input onMouseDown={e => e.stopPropagation()} className="bg-white my-4 focus:outline-none focus:shadow-outline border border-gray-300 rounded-lg py-2 px-4 block w-full appearance-none leading-normal"
                fluid
                label={props.label}
                name={props.name}
                placeholder={props.placeholder}
                type={props.type}
                value={props.value}
                onChange={props.onChange}
              />
            );
};

export default InputField;
