import React from 'react';
import Logged from './Logged'
import NewGrid from './NewGrid'
import Logo from './Logo'
import { Layout, Menu, Affix } from 'antd';
import { UserOutlined, LaptopOutlined, NotificationOutlined, CodeSandboxOutlined, SettingOutlined } from '@ant-design/icons';

const { SubMenu } = Menu;
const { Content, Sider } = Layout;


const Widgetihome = () => {
    return (
    <div>
        <div>
            <Affix offsetTop={0} onChange={affixed => console.log(affixed)}>
                <Logged />
            </Affix>
        </div>
        <div>
        <Content>
            <Layout className="site-layout-background" style={{backgroundColor: '#fff'}}>
                <Sider className="site-layout-background" width={200}>
                <Menu
                    mode="inline"
                    defaultSelectedKeys={['1']}
                    defaultOpenKeys={['sub1']}
                    style={{ height: '100%' }}
                    onClick={({ item, key, keyPath, domEvent }) => { console.log(item) }}
                >
                    <SubMenu key="sub1" icon={<CodeSandboxOutlined />} title="Widgets">
                    <Menu.Item key="1">option1</Menu.Item>
                    <Menu.Item key="2">option2</Menu.Item>
                    <Menu.Item key="3">option3</Menu.Item>
                    <Menu.Item key="4">option4</Menu.Item>
                    <button className="ml-12 cursor-pointer content-center bg-green-400 hover:bg-green-500 text-white font-bold p-4 rounded ">I am a Button</button>
                    </SubMenu>
                    <SubMenu key="sub2" icon={<UserOutlined />} title="User Controll">
                    <Menu.Item key="5">option5</Menu.Item>
                    <Menu.Item key="6">option6</Menu.Item>
                    <Menu.Item key="7">option7</Menu.Item>
                    <Menu.Item key="8">option8</Menu.Item>
                    </SubMenu>
                    <SubMenu key="sub3" icon={<SettingOutlined />} title="Widget Settings">
                    <Menu.Item key="9">option9</Menu.Item>
                    <Menu.Item key="10">option10</Menu.Item>
                    <Menu.Item key="11">option11</Menu.Item>
                    <Menu.Item key="12">option12</Menu.Item>
                    </SubMenu>
                </Menu>
                </Sider>
                <Content style={{overflowY: 'hidden'}}><NewGrid /></Content>
            </Layout>
            </Content>
        </div>
        {/* <Logo></Logo> */}
    </div>
    )
};

export default Widgetihome;