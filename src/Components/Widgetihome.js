import React from 'react';
import Logged from './Logged'
import MyResponsiveGrid from './MyResponsiveGrid'
import Logo from './Logo'


const Widgetihome = () => {
    return (
    <div>
        <div>
            <Logged></Logged>
        </div>
        <MyResponsiveGrid></MyResponsiveGrid>
        {/* <Logo></Logo> */}
    </div>
    )
};

export default Widgetihome;