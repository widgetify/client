import React, {useState} from 'react';
import axios from 'axios';
import { Redirect} from 'react-router'
import { Link } from 'react-router-dom'
import InputField from './InputField'
import Button from './Button'
import SubtleMessge from './SubtleMessge'
import AlertBox from './AlertBox'
import { createBrowserHistory } from "history"

const history = createBrowserHistory();

const SignupForm = () => {
  
  const [form, setValues] = useState({
    name: '',
    email: '',
    password: '',
    repeat_password: ''
  });
  const [passwordsMatch, setPasswordsMatch] = useState(true);
  const [userNameLengthOk, setUserNameLengthOk] = useState(true);
  const [isEmail, setIsEmail] = useState(true);
  const [passwordLengthOk, setPasswordLengthOk] = useState(true);
  const [allFieldsOk, setAllFieldsOk] = useState(true);
  const [errors, setErrors] = useState(null);


  const updateField = (e) => setValues({
    ...form,
    [e.target.name]: e.target.value
  })
    
  const comparePass = (e) => {
    if (form.password !== e.target.value){
      setPasswordsMatch(false)
    } else {
      setPasswordsMatch(true)
    }
  }

  const checkLengthIs2 = (e) => {
    if (e.target.value.length < 2){
      setUserNameLengthOk(false)
    } else {
      setUserNameLengthOk(true)
    }
  }

  const checkLengthIs8 = (e) => {
    if (e.target.value.length < 8){
      setPasswordLengthOk(false)
    } else {
      setPasswordLengthOk(true)
    }
  }

  const checkEmail = (e) => {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (!re.test(String(e.target.value).toLowerCase())){
        setIsEmail(false)
      } else {
        setIsEmail(true)
      }
  }

  const checkForInvalidFields = () => {
    if (!isEmail || !passwordLengthOk || !userNameLengthOk || !passwordsMatch) {
      setAllFieldsOk(false)
    } else {
      setAllFieldsOk(true)
    }
  }
  
  const onSubmit = e => {
    e.preventDefault();
    axios.post('http://' + process.env.REACT_APP_NODE_AUTH_URL + '/api/user/signup', form)
      .then(response => {
        if (response.data.error){
          console.log("Errror")
          setErrors(response.data.error)
        }
        else{
          localStorage.setItem('usertoken', response.data)
          history.goBack()          
          console.log(response)
        }
      })
      .catch(error => {
        console.log(error)
      })
  }
    
    if (localStorage.usertoken){
      return <Redirect to="/" />
    }

  return(
    <div className="flex">
      <div className="w-2/6"></div>
      <div className="my-24 w-2/6">
        <h1>Sign Up</h1>
        
        <Link to='/login'>
          <SubtleMessge>Already have an account? Click here to log in</SubtleMessge>
        </Link> 
              
        <form onSubmit={onSubmit}>

          <InputField label="name" name='name'  placeholder='username' type="text" value={form.name} 
          onChange={(e) => {updateField(e);checkLengthIs2(e); checkForInvalidFields()}}>
          </InputField>

          <InputField label="email" name='email'  placeholder='email' type="email" value={form.email} 
          onChange={(e) => {updateField(e);checkEmail(e); checkForInvalidFields()}}>
          </InputField>

          <InputField label="password" name='password'  placeholder='password' type="password" value={form.password} 
          onChange={(e) => {updateField(e);checkLengthIs8(e); checkForInvalidFields()}}>
          </InputField> 

          {/* <InputField label="confirm password" name='repeat_password'  placeholder='confirm password' type="password" value={form.repeat_password} 
          onChange={(e) => {updateField(e);comparePass(e); checkForInvalidFields()}}>
          </InputField> */}

          <Button type="submit">Submit</Button>
        </form>
      </div>
      <div className="w-2/6 px-10 mt-24">
      {allFieldsOk   ?  null : 
        <AlertBox>
            <ul>
              {passwordsMatch     ?  <li></li> : <li>• Passwords do not match</li>}
              {userNameLengthOk   ?  <li></li> : <li>• User name is too short</li>}
              {isEmail            ?  <li></li> : <li>• Not a valid email</li>}
              {passwordLengthOk   ?  <li></li> : <li>• Password should be at least 8 characters</li>}
            </ul>
        </AlertBox>
      }
      {(errors)?
        <AlertBox>
            <ul>
              {(errors && errors.name)?<li>• {errors.name}</li>: null}
              {(errors && errors.email)?<li>• {errors.email}</li>: null}
              {(errors && errors.password)?<li>• {errors.password}</li>: null}
              {(errors && errors.repeat_password)?<li>• {errors.repeat_password}</li>: null}
            </ul>
        </AlertBox>
      : null}
      </div>
    </div>
  )
}

export default SignupForm