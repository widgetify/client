import React, {useState} from 'react';
import axios from 'axios';
import { Redirect } from 'react-router'
import {Link} from 'react-router-dom'
import InputField from './InputField'
import Button from './Button'
import SubtleMessge from './SubtleMessge'
import AlertBox from './AlertBox'
import { createBrowserHistory } from "history"

const history = createBrowserHistory();

const PasswordResetForm = () => {

  const [form, setValues] = useState({
    email: '',
  });
  const [errors, setErrors] = useState(null);
  
  const [redirect, setRedirect] = useState(false);

  const updateField = (e) => setValues({
    ...form,
    [e.target.name]: e.target.value
  })

  const submitHandler = e => {
    e.preventDefault();
    axios.post('http://' + process.env.REACT_APP_NODE_AUTH_URL + '/api/user/passwordreset/', form)
      .then(response => {
          if (response.data.error){
          // setErrors(response.data.error)
          console.log('response.data.error:', response.data.error)
        }
        else{
          console.log(response.data)
          setRedirect(true)
          // return history.goBack();
        }
      })
      .catch(error => {
          console.log(error)
        })
  }

  if (redirect === true){
    return <Redirect to = '/' />
  }

  return(
  <div className="flex">
    <div className="w-2/6"></div>
    <div className="my-24 w-2/6">
      <h1>Reset your password</h1>
      <Link className = "signup-link" to = '/'><SubtleMessge>Don't want to reset your password? Click here.</SubtleMessge></Link>
      <form onSubmit={submitHandler}>
        <InputField label='Email' name='email' placeholder='email' type="email" value={form.email} onChange={updateField}></InputField>

        <Button type='submit'>Send</Button>
      </form>
    </div>
    <div className="w-2/6">
      {(errors)?
        <AlertBox>
            <ul>
            {(errors && errors.email)?<p color = "pink">{errors.email}</p>: null}
            </ul>
        </AlertBox>
      : null}
    </div>
  </div>
  )
}

export default PasswordResetForm