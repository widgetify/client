import React, { Component } from 'react';
import axios from 'axios';
import CheckboxSwitcher from './partials/CheckboxSwitcher'

class WidgetWhatToDo extends Component {
    
    state = {
        thingstodo: null,
        settings: {
            type: 'all',
            participants: 'all',
            highaccessibility: false,
            lowprice: false
        }
    }

    componentDidMount() {
        this.newThingTodo()
    }

    render() { 
        return (
            <div class="bg-gray-200 p-8">
                <h1 class="text-left text-2xl mb-4">What can I do ?</h1>
                <div class="inline-block mb-4 inline-block relative w-64">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="participants">
                        Participants
                    </label>
                    <select onChange={this.changeParticipants} class="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline" id="participants">
                        <option>all</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center pt-6 px-2 text-gray-700">
                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                    </div>
                </div>
                <div class="inline-block mb-4 inline-block relative w-64">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="type">
                        Type
                    </label>
                    <select onChange={this.changeType} class="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline" id="participants">
                        <option>all</option>
                        <option>education</option>
                        <option>recreational</option>
                        <option>social</option>
                        <option>diy</option>
                        <option>charity</option>
                        <option>cooking</option>
                        <option>relaxation</option>
                        <option>music</option>
                        <option>busywork</option>
                    </select>
                    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center pt-6 px-2 text-gray-700">
                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                    </div>
                </div>
                <div class="flex justify-between">
                    <p class="text-gray-700 font-bold mr-2 capitalize">High Accessibility: </p>
                    <CheckboxSwitcher switchClick={this.toggleHighAccessibility.bind(this)} switch_status={this.state.settings.highaccessibility} />
                </div>
                <div class="flex justify-between">
                    <p class="text-gray-700 font-bold mr-2 capitalize">Low Price: </p>
                    <CheckboxSwitcher switchClick={this.toggleLowPrice.bind(this)} switch_status={this.state.settings.lowprice} />
                </div>

                <h1 class="text-center text-2xl mt-8">You could do:</h1>
                <p class="selectable text-center bg-white rounded-full my-4 p-4 text-sm font-semibold text-gray-700">{this.state.thingstodo}</p>
                <button onClick={this.newThingTodo} class="content-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-4">I want to do another thing</button>
            </div>
        );
    }

    newThingTodo = () => {
        const instance = axios.create({baseURL: 'http://' + process.env.REACT_APP_WHATTODO_URL})
        let params = {}
        //get sendable select values
        if (this.state.settings.type != 'all') {
            params.type = this.state.settings.type
        }
        if (this.state.settings.participants != 'all') {
            params.participants = this.state.settings.sparticipants
        }
        params.lowprice = this.state.settings.lowprice
        params.highaccessibility = this.state.settings.highaccessibility
        instance.get(
            '/api/v1/getRandomTodo', { params }
        )
        .then(res => {
            const thingstodo = res.data.activity;
            this.setState({thingstodo: thingstodo})
            //this.setState({ timezones });
        })
    }

    toggleHighAccessibility = () => {
        this.setState(prevState => ({
            settings: {    
                ...prevState.settings,
                highaccessibility: !prevState.settings.highaccessibility
            }
        }))
    }

    toggleLowPrice = () => {
        this.setState(prevState => ({
            settings: {    
                ...prevState.settings,
                lowprice: !prevState.settings.lowprice
            }
        }))
    }

    changeType = (e) => {
        let newVal = e.target.value
        this.setState(prevState => ({
            settings: {    
                ...prevState.settings,
                type: newVal
            }
        }))
    }

    changeParticipants = (e) => {
        let newVal = e.target.value
        this.setState(prevState => ({
            settings: {    
                ...prevState.settings,
                participants: newVal
            }
        }))
    }
}
 
export default WidgetWhatToDo;