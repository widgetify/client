import React from 'react';
import './styles/app.css'
import Container from './Container';
import { BrowserRouter} from 'react-router-dom';
import { ApolloProvider } from '@apollo/react-hooks';
import ApolloClient from 'apollo-boost';

const client = new ApolloClient({
  uri: 'http://' + process.env.REACT_APP_NODE_AUTH_URL + '/graphql',
});


function App() {
  return (
    <div>
      <ApolloProvider client={client}>
        <BrowserRouter>
          <Container />
        </BrowserRouter>
      </ApolloProvider>
    </div>
    
  );
}

export default App;
