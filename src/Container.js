import React from 'react';
import Widgetihome from './Components/Widgetihome';
import Widgetihomengrid from './Components/Widgetihomengrid';
import LoginForm from './Components/LoginForm';
import SignupForm from './Components/SignupForm';
import PasswordResetForm from './Components/PasswordResetForm';
import NewPasswordForm from './Components/NewPasswordForm';
import VerifyEmail from './Components/VerifyEmail';

import { Route, withRouter, Switch } from 'react-router-dom';

function Container({ location }) {
    return (
            <section className="route-section">
            <Switch location={location}>
                <Route exact path = '/oldgrid' component = {Widgetihome}/>
                <Route exact path = '/' component = {Widgetihomengrid}/>
                <Route exact path = '/signup' component = {SignupForm} />
                <Route exact path = '/login' component = {LoginForm} />
                <Route exact path = '/verifyemail/:id/:token' component = {VerifyEmail} />
                <Route exact path = '/passwordreset' component = {PasswordResetForm} />
                <Route exact path = '/newpassword/:id/:token' component = {NewPasswordForm} />
            </Switch>
            </section>
    );
  }

  export default withRouter(Container);