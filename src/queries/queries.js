import { gql } from "apollo-boost";


const getUser = gql`
        query ($_id: MongoID!) {
            userById(_id: $_id) {
                confirmed
                name
                _id
            }
        }`;


export { getUser };